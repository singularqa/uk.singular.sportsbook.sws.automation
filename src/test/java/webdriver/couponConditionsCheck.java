package webdriver;
import org.testng.annotations.*;
import automation_sws_services.addCoupon;

public class couponConditionsCheck extends GeneralTest{

	
	@Test
	
	public void conditionsCheckCoupon() throws Exception{
		
		addCoupon obj1= new addCoupon();
		obj1.addFreebet(2, Integer.parseInt(prop.getProperty("userId")), 1000, (float) 100.01, (float) 3.40, 3);

    	homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
	    homepage.closeFreebetPopup();
	    
	    //number of selection check
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.selectQAMatch2_odd1();
		homepage.chooseFreebet();
		homepage.selectFreebet();
		homepage.clickBet();
		homepage.closePopupConditionCoupon();
		
		//minimum coeficient per odd
		homepage.selectQAMatch1_odd7();
		homepage.closeLeague();
		homepage.openQALeague1();
		homepage.selectQAMatch1_odd1();
		homepage.clickBet();
		homepage.closePopupConditionCoupon();
		
		//minimum ticket coeficient
		homepage.selectQAMatch1_odd5_6();
		homepage.closeLeague();
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd5();
		homepage.selectQAMatch2_odd5();
		homepage.clickBet();
		homepage.closePopupConditionCoupon();
		
		
		//place  ticket with all conditions satisfied
		homepage.selectQAMatch1_odd1();
		homepage.selectQAMatch2_odd1();
		homepage.closeLeague();
		homepage.openQALeague1();
		homepage.selectQAMatch1_odd1();
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		
		homepage.logout();
		
		
	}

}
