package webdriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class placeVFLSingleTicket extends GeneralTest{

	
	@Test
	
	public void placeVFLSingle () throws Exception{
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.virtualsTab();
	    virtualspage.vfl();
	    virtualspage.vfl_odd_first_match();
	    virtualspage.vfl_odd_second_match();
	    virtualspage.vfl_odd_third_match();
	    homepage.singleTab();
	    homepage.clearStake1();
		homepage.insertStake1("5");
		
		homepage.clearStake2();
		homepage.insertStake2("6");

		homepage.clearStake3();
		homepage.insertStake3("8");
		
		homepage.clickBet();
		
		
		int placed = homepage.numberOfSuccesfullyPlacedSingles();
		
		homepage.closePopupTicketPlacedSuccessfullySingle();
	
		
		Assert.assertEquals(placed,3);
		
		
		
	}

}
