package webdriver;
import org.testng.annotations.Test;


public class addOddPrematchToLive extends GeneralTest {
	
	
	@Test
	public void AdditionalOddPrematchToLive() throws Exception{
		
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.liveTab();
		livepage.select_live_odds(1);
		homepage.clearStake();
		homepage.insertStake("5");
		livepage.acceptChanges();
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.historyTab();
		historypage.addOdd();
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfullyNonStop();
		homepage.logout();
		
		
	}

}
