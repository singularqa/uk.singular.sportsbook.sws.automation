package webdriver;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Parameters;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import automation_sws_pages.HistoryPage;
import automation_sws_pages.HomePage;
import automation_sws_pages.LivePage;
import automation_sws_pages.VirtualsPage;


public class GeneralTest {

	protected HomePage homepage;
	protected VirtualsPage virtualspage;
	protected LivePage livepage;
	protected HistoryPage historypage;
	protected WebDriver  driver;
	public static Properties prop = new Properties();

    //create a constructor with browser
    @Parameters("browser")
	@BeforeClass
	public void Initialisation (String browser) throws  MalformedURLException{
		
		readData();
		if(browser.equalsIgnoreCase("firefox")){

			DesiredCapabilities cap = DesiredCapabilities.firefox();
		    cap.setBrowserName("firefox");
	  		driver = new RemoteWebDriver(new URL(prop.getProperty("mozilaNode")),cap);
	  		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			} 
			 else 
				 
		if(browser.equalsIgnoreCase("chrome")){

            
			DesiredCapabilities cap = DesiredCapabilities.chrome();
			cap.setBrowserName("chrome");
  			driver = new RemoteWebDriver(new URL(prop.getProperty("chromeNode")),cap);
  			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  			
			}
		
		homepage = PageFactory.initElements(driver, HomePage.class);
		virtualspage = PageFactory.initElements(driver, VirtualsPage.class);
		livepage = PageFactory.initElements(driver, LivePage.class);
		historypage = PageFactory.initElements(driver, HistoryPage.class);
		driver.get(prop.getProperty("basicUrl"));
		driver.manage().window().maximize();
		
	}
	public void readData(){
		// Create FileInputStream Object  
				File file = new File("data\\environment_setup.properties");  
				 FileInputStream fileInput = null;
					try {
						fileInput = new FileInputStream(file);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}			
					//load properties file
					try {
						prop.load(fileInput);
					} catch (IOException e) {
						e.printStackTrace();
					}
	}
	@AfterClass
	public void afterTest() {
		
			driver.close();
			driver.quit();
		
	}
}
