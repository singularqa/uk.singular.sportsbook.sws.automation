package webdriver;
import org.testng.annotations.*;

public class noBalancePlaceTicket extends GeneralTest {

	
	@Test
	public void NoBalancePlaceTicket () throws Exception{
		
		homepage.login(prop.getProperty("user0balance"), prop.getProperty("pass0balance"));
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.selectQAMatch2_odd1();
		homepage.closeLeague();
		homepage.openQALeague1();
		homepage.selectQAMatch1_odd1();
		homepage.clearStake();
		homepage.insertStake("5");
		homepage.clickBet();
		homepage.closePopupNoMoneyOnBalance();
		homepage.logout();
	}
	

}
