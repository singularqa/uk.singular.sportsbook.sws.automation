package webdriver;

import org.testng.Assert;
import org.testng.annotations.*;
public class checkBalanceAfterTicketPlacing extends GeneralTest{

	
	@Test
	public void CheckBalance () throws Exception{
		int Stake=10;
		int Balance_before;
		int Balance_after;
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.selectQAMatch2_odd1();
		homepage.clearStake();
		homepage.insertStake(Integer.toString(Stake));
		Balance_before=homepage.checkUserBalance();
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		Balance_after=homepage.checkUserBalance();
		
		Assert.assertEquals(Balance_before,Balance_after+Stake);
		homepage.logout();
	
		
	}

}
