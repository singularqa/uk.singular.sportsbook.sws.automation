package webdriver;
import org.testng.annotations.*;

public class placePrematchMultiTicket extends GeneralTest {

	@Test
	public void PlaceTicket () throws Exception{
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.selectQAMatch2_odd1();
		homepage.closeLeague();
		homepage.openQALeague1();
		homepage.selectQAMatch1_odd1();
		homepage.clearStake();
		homepage.insertStake("5");
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.logout();
	}
	

}
