package webdriver;
import org.testng.annotations.*;

public class minBetAmountMulti extends GeneralTest{

	
	@Test
	public void minBetAmount() throws Exception{
		float minBetAmount;
		float minBetAmountUnder;
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.selectQAMatch2_odd1();
		homepage.closeLeague();
		homepage.openQALeague1();
		homepage.selectQAMatch1_odd1();
		homepage.clearStake();
		homepage.insertStake("0");
		homepage.clickBet();
		minBetAmount=homepage.minBetAmount();
		minBetAmountUnder=(float) (minBetAmount-0.01);
		homepage.closePopupMinBetAmount();
		homepage.clearStake();
		homepage.insertStake(Float.toString(minBetAmountUnder));
		homepage.clickBet();
		homepage.closePopupMinBetAmount();
		homepage.clearStake();
		homepage.insertStake(Float.toString(minBetAmount));
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		
	
		
		
	}
}
