package webdriver;

import org.testng.Assert;
import org.testng.annotations.*;
import automation_sws_services.addCoupon;

public class checkBalanceAfterTicketPlacingWithCoupon extends GeneralTest{

	
	@Test
	public void CheckBalanceCoupon () throws Exception{
		addCoupon obj1= new addCoupon();
		obj1.addFreebet(2, Integer.parseInt(prop.getProperty("userId")), 15, (float) 1.51, (float) 1.10, 1);
		int Balance_before;
		int Balance_after;
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.closeFreebetPopup();
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.selectQAMatch2_odd1();
		
		homepage.chooseFreebet();
		homepage.selectFreebet();
		Balance_before=homepage.checkUserBalance();
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		Balance_after=homepage.checkUserBalance();
		
		Assert.assertEquals(Balance_before,Balance_after);
		homepage.logout();
	
		
	}

}
