package webdriver;
import org.testng.annotations.*;

public class limitAskForConfirmationNoAction extends GeneralTest {


	
	@Test
	
	public void askForConfirmationNoAction() throws Exception{
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague1();
		homepage.selectQAMatch1_odd1();
		homepage.clearStake();
		homepage.insertStake("70");
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.logout();
		
	}

}
