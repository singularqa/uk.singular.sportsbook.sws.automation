package webdriver;
import org.testng.Assert;
import org.testng.annotations.*;


public class placePrematchSingleTicket extends GeneralTest{

	
	@Test
	public void placeSingle() throws Exception {
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.selectQAMatch2_odd1();
		homepage.closeLeague();
		homepage.openQALeague1();
		homepage.selectQAMatch1_odd1();
		homepage.singleTab();
		
		homepage.clearStake1();
		homepage.insertStake1("5");
		
		homepage.clearStake2();
		homepage.insertStake2("6");

		homepage.clearStake3();
		homepage.insertStake3("8");
		
		homepage.clickBet();
		
		
		int placed = homepage.numberOfSuccesfullyPlacedSingles();
		
		homepage.closePopupTicketPlacedSuccessfullySingle();
		homepage.logout();
		
		Assert.assertEquals(placed,3);

		
	}

	
}
