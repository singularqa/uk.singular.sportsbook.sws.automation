package webdriver;
import org.testng.annotations.Test;


public class placeVirtualAndPrematch extends GeneralTest {
	
	
	@Test
	public void PlaceVirtualAndPrematch() throws Exception{
		
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.virtualsTab();
	    virtualspage.vfl();
	    virtualspage.vfl_odd_first_match();
	    homepage.clearStake();
		homepage.insertStake("5");
		homepage.clickBet();
	    historypage.closePopupCannotMixVirtualAndPrematch();
		homepage.logout();
		
		
	}

}
