 package webdriver;
import org.testng.annotations.*;

public class placeVirtualsHybridMultiTicket extends GeneralTest {

	
	@Test
	public void placeVFLMulti () throws Exception{
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.virtualsTab();
	    virtualspage.vfl();
	    virtualspage.vfl_odd_first_match();
	    virtualspage.vfl_euroCup();
	    virtualspage.vfl_odd_first_match();
	    virtualspage.vfl_nationsCup();
	    virtualspage.vfl_odd_first_match();
	    virtualspage.vbl();
	    virtualspage.vbl_odd_first_match();
	    virtualspage.vtl();
	    virtualspage.vtl_odd_first_match();
	    homepage.clearStake();
	    homepage.insertStake("5");
	    homepage.clickBet();
	    homepage.closePopupTicketPlacedSuccessfully();

	    
		
	}

}
