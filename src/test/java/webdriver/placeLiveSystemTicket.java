package webdriver;

import org.testng.annotations.*;

public class placeLiveSystemTicket extends GeneralTest {

	@Test
	public void PlaceLiveSystemTicket () throws Exception{

		 
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.liveTab();
		livepage.select_live_odds(3);	
		homepage.systemTab();
		homepage.plusOneSystem();
		homepage.systemMatchInsertBanker();
		homepage.clearStake();
		homepage.insertStake("10");
		livepage.acceptChanges();
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.logout();
		
	}}
