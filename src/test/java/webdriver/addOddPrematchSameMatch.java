package webdriver;
import org.testng.annotations.Test;


public class addOddPrematchSameMatch extends GeneralTest {
	
	
	@Test
	public void AdditionalOddPrematchSameMatch() throws Exception{
		
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.clearStake();
		homepage.insertStake("5");
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.historyTab();
		historypage.addOdd();
		homepage.selectQAMatch1_odd1();
		homepage.clickBet();
		homepage.closePopupOddExistOnTicket();
		homepage.logout();
		
		
	}

}
