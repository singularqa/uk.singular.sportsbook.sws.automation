package webdriver;


import org.testng.annotations.Test;

public class cashoutLive extends GeneralTest{

	
	@Test
	public void CashoutLive () throws Exception{

		 
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.liveTab();
		livepage.select_live_odds(1);
		homepage.clearStake();
		homepage.insertStake("30");
		livepage.acceptChanges();
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.historyTab();
		historypage.cashOutButton();
		Thread.sleep(8000);
		historypage.makeCashout();
		Thread.sleep(9000);
		historypage.closePopupCashoutSuccessful();
		homepage.logout();
		
		
	}
}
