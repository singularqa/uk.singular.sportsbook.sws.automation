package webdriver;
import org.testng.annotations.*;

public class placePrematchSystemTicket extends GeneralTest{

	
	@Test
	
	public void placeSystemTicket() throws Exception{
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.selectQAMatch2_odd1();
		homepage.closeLeague();
		homepage.openQALeague1();
		homepage.selectQAMatch1_odd1();
		homepage.systemTab();
		homepage.plusOneSystem();
		homepage.systemMatchInsertBanker();
		homepage.clearStake();
		homepage.insertStake("10");
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.logout();
		
	}
	

}
