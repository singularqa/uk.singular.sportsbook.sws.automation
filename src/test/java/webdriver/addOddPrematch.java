package webdriver;
import org.testng.annotations.Test;


public class addOddPrematch extends GeneralTest {
	
	
	@Test
	public void AdditionalOddPrematch() throws Exception{
		
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.clearStake();
		homepage.insertStake("5");
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.historyTab();
		historypage.addOdd();
		homepage.selectQAMatch2_odd1();
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfullyNonStop();
		homepage.logout();
		
		
	}

}
