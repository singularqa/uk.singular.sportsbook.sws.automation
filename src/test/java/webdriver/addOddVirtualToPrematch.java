package webdriver;
import org.testng.annotations.Test;


public class addOddVirtualToPrematch extends GeneralTest {
	
	
	@Test
	public void AdditionalOddVirtualToPrematch() throws Exception{
		
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.clearStake();
		homepage.insertStake("5");
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.historyTab();
		historypage.addOdd();
		homepage.virtualsTab();
	    virtualspage.vfl();
	    virtualspage.vfl_odd_first_match();
	    homepage.clickBet();
	    historypage.closePopupCannotMixVirtualAndPrematch();
		homepage.logout();
		
		
	}

}
