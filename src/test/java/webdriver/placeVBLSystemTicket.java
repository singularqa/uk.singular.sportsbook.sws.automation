package webdriver;
import org.testng.annotations.*;

public class placeVBLSystemTicket extends GeneralTest {

	
	@Test
	
	public void placeVBLSystem() throws Exception{
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.virtualsTab();
	    virtualspage.vbl();
	    virtualspage.vbl_odd_first_match();
	    virtualspage.vbl_odd_second_match();
	    virtualspage.vbl_odd_third_match();
		homepage.systemTab();
		homepage.plusOneSystem();
		homepage.systemMatchInsertBanker();
		homepage.clearStake();
		homepage.insertStake("10");
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		
		
	}
	
	

}
