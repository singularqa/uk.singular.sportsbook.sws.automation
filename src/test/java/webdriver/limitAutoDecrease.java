package webdriver;
import org.testng.annotations.*;

public class limitAutoDecrease extends GeneralTest{


	
	@Test
	
	public void AutoDecrease() throws Exception{
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague2();
		homepage.selectQAMatch2_odd1();
		homepage.clearStake();
		homepage.insertStake("1001");
		homepage.clickBet();
		homepage.closePopupOddsWereChanged();
		homepage.placeBetPopup();
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.logout();
		
	}
}
