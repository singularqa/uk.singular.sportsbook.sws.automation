package webdriver;
import org.testng.annotations.*;

public class placeVFLMultiTicket extends GeneralTest {

	
	@Test
	public void placeVFLMulti () throws Exception{
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.virtualsTab();
	    virtualspage.vfl();
	    virtualspage.vfl_odd_first_match();
	    virtualspage.vfl_odd_second_match();
	    virtualspage.vfl_odd_third_match();
	    homepage.clearStake();
	    homepage.insertStake("5");
	    homepage.clickBet();
	    homepage.closePopupTicketPlacedSuccessfully();

	    
		
	}

}
