package webdriver;
import org.testng.annotations.*;

public class placePrematchAfterLogOut extends GeneralTest {


	@Test
	
	public void placeAfterLogin () throws Exception{
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.logout();
		homepage.clearStake();
		homepage.insertStake("5");
		homepage.clickBet();
		homepage.closePopupPleaseAuthorize();
		
	}
	
}
