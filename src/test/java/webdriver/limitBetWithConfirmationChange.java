package webdriver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import org.testng.annotations.*;

import automation_sws_services.bookmakerAcceptTicket;
import automation_sws_services.bookmakerChangeOdds;

public class limitBetWithConfirmationChange extends GeneralTest {

	private static Connection conn;
	private static ResultSet query_result;
	private static ResultSet query_result2;
	@Test
	
	public void betWithrConfirmationChange() throws Exception{
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague1();
		homepage.selectQAMatch1_odd1();
		homepage.clearStake();
		homepage.insertStake("1001");
		homepage.clickBet();
		Thread.sleep(1000);
		try {

			conn = DriverManager.getConnection("jdbc:mysql://172.16.41.31:3306/sportsbook", "sportsbook",
					"tGa8epawPs4JaQz2");
			Statement stmt = conn.createStatement();
			Statement stmt2=conn.createStatement();
			
			query_result = stmt.executeQuery("select id from tickets where player_id="+prop.getProperty("SBid")+" order by id desc limit 1;");
			query_result2 = stmt2.executeQuery("select id from ticket_odds where ticket_id= (select id from tickets where player_id="+prop.getProperty("SBid")+" order by id desc limit 1);");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String ticket_id = "";
		String odd_id = "";
		
		while (query_result.next()) {
		ticket_id=query_result.getString("id");
		}
		
		while (query_result2.next()) {
			odd_id=query_result2.getString("id");
			}
		
		bookmakerChangeOdds obj1 = new bookmakerChangeOdds();
		obj1.bookmakerChangeTickett(ticket_id, odd_id);
		
		homepage.closePopupOddsWereChanged();
		homepage.placeBetPopup();
		Thread.sleep(1000);
		bookmakerAcceptTicket obj2 = new bookmakerAcceptTicket();
		obj2.bookmakerAcceptTickett(ticket_id);
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.logout();
		
	}

}
