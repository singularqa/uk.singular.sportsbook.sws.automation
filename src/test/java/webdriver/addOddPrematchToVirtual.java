package webdriver;
import org.testng.annotations.Test;


public class addOddPrematchToVirtual extends GeneralTest {
	
	
	@Test
	public void AdditionalOddPrematchToVirtual() throws Exception{
		
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.virtualsTab();
	    virtualspage.vfl();
	    virtualspage.vfl_odd_first_match();
	    homepage.clearStake();
	    homepage.insertStake("5");
	    homepage.clickBet();
	    homepage.closePopupTicketPlacedSuccessfully();
		homepage.historyTab();
		historypage.addOdd();
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.clickBet();
		historypage.closePopupCannotMixVirtualAndPrematch();
		homepage.logout();
		
		
	}

}
