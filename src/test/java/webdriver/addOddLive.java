package webdriver;
import org.testng.annotations.Test;


public class addOddLive extends GeneralTest {
	
	
	@Test
	public void AdditionalOddLive() throws Exception{
		
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.liveTab();
		livepage.select_live_odds(1);
		homepage.clearStake();
		homepage.insertStake("5");
		livepage.acceptChanges();
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.historyTab();
		historypage.addOdd();
		homepage.liveTab();
		livepage.select_live_odds_Starting_from_seconds_match(1);
		livepage.acceptChanges();
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfullyNonStop();
		homepage.logout();
		
		
	}

}
