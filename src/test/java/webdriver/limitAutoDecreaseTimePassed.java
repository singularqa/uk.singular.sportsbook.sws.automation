package webdriver;
import org.testng.annotations.*;

public class limitAutoDecreaseTimePassed extends GeneralTest{


	@Test
	
	public void AutoDecreaseTimePassed() throws Exception{
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague2();
		homepage.selectQAMatch2_odd1();
		homepage.clearStake();
		homepage.insertStake("1001");
		homepage.clickBet();
		homepage.closePopupOddsWereChanged();
		Thread.sleep(21000);
		homepage.placeBetPopup();
		homepage.closePopupTimeForAcceptingPassed();
		homepage.logout();
		
	}
}
