package webdriver;
import org.testng.annotations.*;

public class placeVFL_EuroCupSingleTicket extends GeneralTest {

	
	@Test
	public void placeVFL_EuroCupSingle () throws Exception{
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.virtualsTab();
	    virtualspage.vfl_euroCup();
	    virtualspage.vfl_odd_first_match();
	    homepage.clearStake();
	    homepage.insertStake("5");
	    homepage.clickBet();
	    homepage.closePopupTicketPlacedSuccessfully();

	    
		
	}

}
