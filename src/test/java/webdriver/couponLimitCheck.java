package webdriver;
import org.testng.annotations.*;
import automation_sws_services.addCoupon;

public class couponLimitCheck extends GeneralTest {

	
	@Test
	
	public void couponLimitHit() throws Exception{
		addCoupon obj1= new addCoupon();
		obj1.addFreebet(2, Integer.parseInt(prop.getProperty("userId")), 100100, (float) 1.51, (float) 1.10, 1);

    	homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
	    homepage.closeFreebetPopup();
		homepage.openQALeague2();
		homepage.selectQAMatch2_odd1();
		homepage.chooseFreebet();
		homepage.selectFreebet();
		homepage.clickBet();
		homepage.closePopupOddsWereChanged();
		homepage.placeBetPopup();
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.logout();
		
		
	}

}
