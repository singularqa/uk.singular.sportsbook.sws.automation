package webdriver;
import org.testng.annotations.*;

public class placeVBLMultiTicket extends GeneralTest {

	
	@Test
	public void placeVBLMulti () throws Exception{
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.virtualsTab();
	    virtualspage.vbl();
	    virtualspage.vbl_odd_first_match();
	    virtualspage.vbl_odd_second_match();
	    virtualspage.vbl_odd_third_match();
	    homepage.clearStake();
	    homepage.insertStake("5");
	    homepage.clickBet();
	    homepage.closePopupTicketPlacedSuccessfully();
	    
		
	}

}
