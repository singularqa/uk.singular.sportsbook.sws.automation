package webdriver;
import org.testng.annotations.Test;


public class addOddLiveToPrematch extends GeneralTest {
	
	
	@Test
	public void AdditionalOddLiveToPrematch() throws Exception{
		
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.clearStake();
		homepage.insertStake("5");
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.historyTab();
		historypage.addOdd();
		homepage.liveTab();
		livepage.select_live_odds(1);
		livepage.acceptChanges();
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfullyNonStop();
		homepage.logout();
		
		
	}

}
