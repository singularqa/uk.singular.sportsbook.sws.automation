package webdriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class placeVBLSingleTicket extends GeneralTest{

	@Test
	
	public void placeVBLSingle () throws Exception{
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.virtualsTab();
	    virtualspage.vbl();
	    virtualspage.vbl_odd_first_match();
	    virtualspage.vbl_odd_second_match();
	    virtualspage.vbl_odd_third_match();
	    homepage.singleTab();
	    homepage.clearStake1();
		homepage.insertStake1("5");
		
		homepage.clearStake2();
		homepage.insertStake2("6");

		homepage.clearStake3();
		homepage.insertStake3("8");
		
		homepage.clickBet();
		
		
		int placed = homepage.numberOfSuccesfullyPlacedSingles();
		
		homepage.closePopupTicketPlacedSuccessfullySingle();
	
		Assert.assertEquals(placed,3);
		
		
		
	}

}
