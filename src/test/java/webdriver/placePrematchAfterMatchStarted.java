package webdriver;


import org.testng.annotations.*;

public class placePrematchAfterMatchStarted extends GeneralTest{

	
	@Test
	
	public void PlaceTicketAfterMatchStarted() throws Exception{
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague1();
		homepage.selectQAMatch1_odd1();
		homepage.clearStake();
		homepage.insertStake("5");
		homepage.make_match_started();
		homepage.clickBet();
		homepage.closePopupMatchBeenDisabled();
		homepage.return_match_date();
		homepage.logout();

	}
	
}
