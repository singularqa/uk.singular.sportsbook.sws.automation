package webdriver;
import org.testng.annotations.Test;


public class addOddPrematchToResolvedTicket extends GeneralTest {
	
	
	@Test
	public void AdditionalOddPrematchToResolvedTicket() throws Exception{
		
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.clearStake();
		homepage.insertStake("5");
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.historyTab();
		historypage.resolve_ticket_winnig();
		historypage.addOdd();
		homepage.selectQAMatch2_odd1();
		homepage.clickBet();
		historypage.closePopupForbidenToAddAdditionalOdds();
		homepage.logout();
		
		
	}

}
