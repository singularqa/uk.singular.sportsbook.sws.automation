package webdriver;
import org.testng.annotations.*;
import automation_sws_services.addCoupon;

public class couponPlaceLive extends GeneralTest {

	
	
	@Test
	
	public void couponPlaceLiveTicket() throws Exception{
		
		addCoupon obj1= new addCoupon();
		obj1.addFreebet(2, Integer.parseInt(prop.getProperty("userId")), 15, (float) 1.01, (float) 1.01, 1);

    	homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
	    homepage.closeFreebetPopup();
	    homepage.liveTab();
		livepage.select_live_odds(3);
		homepage.chooseFreebet();
		homepage.selectFreebet();
		livepage.acceptChanges();
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		
		
	}

}
