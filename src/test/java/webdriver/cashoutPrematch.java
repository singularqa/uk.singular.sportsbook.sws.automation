package webdriver;


import org.testng.annotations.Test;

public class cashoutPrematch extends GeneralTest{

	
	@Test
	public void CashoutPrematch () throws Exception{

		 
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague2();
		homepage.selectQAMatch2_odd7();
		homepage.clearStake();
		homepage.insertStake("5");
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		homepage.historyTab();
		historypage.cashOutButton();
		historypage.makeCashout();
		Thread.sleep(1000);
		historypage.closePopupCashoutSuccessful();
		homepage.logout();
		
		
	}
}
