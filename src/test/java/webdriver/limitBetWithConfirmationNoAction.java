package webdriver;
import org.testng.annotations.*;

public class limitBetWithConfirmationNoAction extends GeneralTest {

	@Test
	
	public void betWithConfirmationNoActions() throws Exception{
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.openQALeague1();
		homepage.selectQAMatch1_odd1();
		homepage.clearStake();
		homepage.insertStake("1001");
		homepage.clickBet();
		homepage.closePopupTicketWasRejected();
		homepage.logout();
		
	}

}
