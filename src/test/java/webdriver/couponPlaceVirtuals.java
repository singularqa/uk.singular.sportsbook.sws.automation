package webdriver;

import org.testng.annotations.*;

import automation_sws_services.addCoupon;

public class couponPlaceVirtuals extends GeneralTest {

	
	@Test
	
	public void couponPlaceVirtual() throws Exception{
		
		addCoupon obj1= new addCoupon();
		obj1.addFreebet(2, Integer.parseInt(prop.getProperty("userId")), 15, (float) 1.01, (float) 1.01, 1);

		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.closeFreebetPopup();
		homepage.virtualsTab();
		virtualspage.vbl();
	    virtualspage.vbl_odd_first_match();
	    virtualspage.vbl_odd_second_match();
	    virtualspage.vbl_odd_third_match();
		homepage.chooseFreebet();
		homepage.selectFreebet();
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
		
		
	}
}