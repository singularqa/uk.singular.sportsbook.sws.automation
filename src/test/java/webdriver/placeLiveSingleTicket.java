package webdriver;

import org.testng.Assert;
import org.testng.annotations.*;



public class placeLiveSingleTicket extends GeneralTest {

	@Test
	public void PlaceLiveSingleTicket () throws Exception{

		 
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.liveTab();
		livepage.select_live_odds(2);
		homepage.singleTab();
		homepage.clearStake1();
		homepage.insertStake1("5");
		homepage.clearStake2();
		homepage.insertStake2("6");
		livepage.acceptChanges();
		homepage.clickBet();
		
		Thread.sleep(2000);
		
		int placed = livepage.numberOfSuccesfullyPlacedSingles();
		
		homepage.closePopupTicketPlacedSuccessfullySingle();
		homepage.logout();
		
		Assert.assertEquals(placed,2);
		
		
	}
	
}
