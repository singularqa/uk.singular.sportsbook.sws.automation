package webdriver;
import org.testng.annotations.*;

public class placeVFLSystemTicket extends GeneralTest {

	
	@Test
	public void placeVFLSystem() throws Exception{
		
		
		homepage.login(prop.getProperty("user"), prop.getProperty("pass"));
		homepage.virtualsTab();
	    virtualspage.vfl();
	    virtualspage.vfl_odd_first_match();
	    virtualspage.vfl_odd_second_match();
	    virtualspage.vfl_odd_third_match();
		homepage.systemTab();
		homepage.plusOneSystem();
		homepage.systemMatchInsertBanker();
		homepage.clearStake();
		homepage.insertStake("10");
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
	
		
	}
	
	

}
