package webdriver;
import org.testng.annotations.*;
import automation_sws_services.addCoupon;

public class noBalanceCouponTicketPlace extends GeneralTest {

	@Test
	public void NoBalancePlaceTicketCoupon() throws Exception{
		
		addCoupon obj1= new addCoupon();
		obj1.addFreebet(2, Integer.parseInt(prop.getProperty("user0balanceUserId")), 15, (float) 1.01, (float) 1.01, 1);

		homepage.login(prop.getProperty("user0balance"), prop.getProperty("pass0balance"));
		homepage.closeFreebetPopup();
		homepage.openQALeague2();
		homepage.selectQAMatch1_odd1();
		homepage.selectQAMatch2_odd1();
		homepage.closeLeague();
		homepage.openQALeague1();
		homepage.selectQAMatch1_odd1();
		homepage.chooseFreebet();
		homepage.selectFreebet();
		homepage.clickBet();
		homepage.closePopupTicketPlacedSuccessfully();
	}
	

}
