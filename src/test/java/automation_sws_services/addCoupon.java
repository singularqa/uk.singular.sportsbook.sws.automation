package automation_sws_services;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import webdriver.GeneralTest;


public class addCoupon {
	
	
	
	public void addFreebet(int currency, int playerId, int betAmount, float minCoefficient, float minCoefficientPerOdd, int minNumberOfSelections) throws Exception{
		
		String ProviderID=GeneralTest.prop.getProperty("ProviderID");
		String SecretKey=GeneralTest.prop.getProperty("SecretKey");
		 
		
		String hash= ProviderID+ Integer.toString(playerId)+ Integer.toString(betAmount)+ Float.toString(minCoefficient)+ SecretKey;
		
		String md5hash = DigestUtils.md5Hex(hash);
		
	    //System.out.println(hash);
		//System.out.println(md5hash);
	
		
		HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(GeneralTest.prop.getProperty("basicUrlApi")+"rest/promotion/ext/freebet/create?pId="+ProviderID+"&hash="+md5hash+"");

        post.addHeader("Content-Type", "application/json");

        String params = "{\n" +
                "            \"betAmount\":"+betAmount+"," +
                "            \"currency\":"+currency+"," +
                "            \"startDate\":\"2017-01-01\"," +
                "            \"endDate\":\"2020-02-01\"," +
                "            \"enabled\":1," +
                "            \"playerId\":"+playerId+"," +
                "            \"couponType\":\"FreeBet\"," +
                "            \"minCoefficient\":"+minCoefficient+"," +
                "            \"minCoefficientPerOdd\":"+ minCoefficientPerOdd+"," +
                "            \"minNumberOfSelections\":"+minNumberOfSelections+"" +
                "        }";
        StringEntity entity = new StringEntity(params, ContentType.APPLICATION_JSON);
       post.setEntity(entity);
    //HttpResponse response =
       client.execute(post);

          /*  BufferedReader rd = new BufferedReader(
                     new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
             String line = "";
             while ((line = rd.readLine()) != null) {
                 result.append(line);
             }

            System.out.println(result);
            
        */
		
	}

}
