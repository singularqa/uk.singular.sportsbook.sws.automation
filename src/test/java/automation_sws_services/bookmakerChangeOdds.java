package automation_sws_services;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import webdriver.GeneralTest;

public class bookmakerChangeOdds {

	
public void bookmakerChangeTickett(String ticketID,String oddId) throws Exception{
		
		bookmakerToken tok = new bookmakerToken();
		String token = tok.getTokenBookmaker();
		String odd = "1.01";
		
		String authenticationBookmekerURL= GeneralTest.prop.getProperty("basicUrlApi")+"rest/ticket/change";

        HttpClient client = HttpClientBuilder.create().build();
        CookieStore cookieStore = new BasicCookieStore();
        HttpContext httpContext = new BasicHttpContext();
        httpContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
        HttpPost post = new HttpPost(authenticationBookmekerURL);

        post.addHeader("Content-Type", "application/json");
        post.addHeader("Authorization", "Bearer "+token+"");
        
  
        String params = "{" +
                "  \"id\":"+ticketID+"," +
                "  \"ticketOdds\": [" +
                "    {" +
                "      \"id\":"+oddId+"," +
                "      \"value\": "+odd+"" +
                "    }]" +
                "}";
        
        StringEntity entity = new StringEntity(params, ContentType.APPLICATION_JSON);
       post.setEntity(entity);
       
       HttpResponse response = client.execute(post, httpContext);
        
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

       StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        
     
      //  System.out.println(result);
		
	}
}
