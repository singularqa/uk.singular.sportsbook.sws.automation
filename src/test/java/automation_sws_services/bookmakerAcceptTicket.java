package automation_sws_services;


import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import webdriver.GeneralTest;


public class bookmakerAcceptTicket {


	public void bookmakerAcceptTickett(String ticketID) throws Exception{
		
		bookmakerToken tok = new bookmakerToken();
		String token = tok.getTokenBookmaker();
		
		String authenticationBookmekerURL= GeneralTest.prop.getProperty("basicUrlApi")+"rest/ticket/accept?id="+ticketID+"";

        HttpClient client = HttpClientBuilder.create().build();
        CookieStore cookieStore = new BasicCookieStore();
        HttpContext httpContext = new BasicHttpContext();
        httpContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
        HttpPost post = new HttpPost(authenticationBookmekerURL);

        post.addHeader("Content-Type", "application/x-www-form-urlencoded");
        post.addHeader("Authorization", "Bearer "+token+"");
        
  
       
       HttpResponse response = client.execute(post, httpContext);
        
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

       StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        
     
     //   System.out.println(result);
		
	}
}
