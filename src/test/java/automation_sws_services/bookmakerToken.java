package automation_sws_services;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import webdriver.GeneralTest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class bookmakerToken {
	
	

	public String getTokenBookmaker() throws Exception {

		String authenticationBookmekerURL= GeneralTest.prop.getProperty("basicUrlApi")+"rest/oauth2/token";

        HttpClient client = HttpClientBuilder.create().build();
        CookieStore cookieStore = new BasicCookieStore();
        HttpContext httpContext = new BasicHttpContext();
        httpContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
        HttpPost post = new HttpPost(authenticationBookmekerURL);

        post.addHeader("Content-Type", "application/x-www-form-urlencoded");
        
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("client_id", "Z6fINbl1EeOmuwAFmjx4AA=="));
        urlParameters.add(new BasicNameValuePair("grant_type", "password"));
        urlParameters.add(new BasicNameValuePair("username", "shako"));
        urlParameters.add(new BasicNameValuePair("password", "shako1992"));



        post.setEntity(new UrlEncodedFormEntity(urlParameters));

        HttpResponse response = client.execute(post, httpContext);
        
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

       StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        
        JsonParser jsonParser = new JsonParser();
        JsonObject json = jsonParser.parse(result.toString())
                .getAsJsonObject();
        String token = json.get("access_token").getAsString();
       // System.out.println(token);

        return token;


    }

       
}
