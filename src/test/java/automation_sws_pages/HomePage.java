package automation_sws_pages;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import webdriver.GeneralTest;

public class HomePage extends GeneralTestPage{
	private static Connection conn;
	
	@FindBy(css = "li[id='Menu_virtual']")
	WebElement virtualsTab;
	
	@FindBy(css = "li[id='Menu_superlive']")
	WebElement liveTab;
	
	@FindBy(css = "li[id='Menu_history']")
	WebElement historyTab;
	
	@FindBy(css = "input[name='username']")
	WebElement usernameField;
	
	@FindBy(css = "input[name='password']")
	WebElement passwordField;
	
	@FindBy( css = "button[type='submit']")
	WebElement loginButton;
	
	@FindBy( css = "div > input[onclick = 'Ticket.clear();'")
	WebElement clearBetslipButton;
	
	@FindBy( css = "div > input[value= 'Yes'")
	WebElement confirmClear;
	
	@FindBy( css = "a.logout-btn")
	WebElement logoutButton;
	
	@FindBy( xpath = ".//*[@id='PopUp']/div/div[3]/div[2]/input")
	WebElement closePopup;
	
	@FindBy( css = "#PopUp > div > div.message-box-actions > div > input")
	WebElement closePopupOK;
	
	@FindBy( xpath = ".//*[@id='PopUp']/div/div[2]")
	WebElement message;
	
	@FindBy( css = "div[class='message-box-content']  > p")
	WebElement messageChanged;
	
	@FindBy( xpath = ".//table/tbody/tr[1]/td[3]")
	WebElement QAMatch1_odd1;
	
	@FindBy( xpath = ".//table/tbody/tr[1]/td[5]")
	WebElement QAMatch1_odd5_6;
	
	@FindBy( xpath = ".//table/tbody/tr[1]/td[7]")
	WebElement QAMatch1_odd5;
	
	@FindBy( xpath = ".//table/tbody/tr[1]/td[9]")
	WebElement QAMatch1_odd7;
	
	@FindBy( xpath = ".//table/tbody/tr[3]/td[3]")
	WebElement QAMatch2_odd1;

	@FindBy( xpath = ".//table/tbody/tr[3]/td[7]")
	WebElement QAMatch2_odd5;
	
	@FindBy( xpath = ".//table/tbody/tr[3]/td[9]")
	WebElement QAMatch2_odd7;
	
	@FindBy( css = "div[id='categoryMenu']")
	WebElement sportsbookTree;

	@FindBy( css = "input[id='StakeValue']")
	WebElement stakeField;
	
	@FindBy( css = "div > input[onclick = 'Ticket.bet()']")
	WebElement betButton;
	
	@FindBy( css = "div.logout-btn.balance.visible")
	WebElement userBalance;
	
	@FindBy( css = "a[title='Close']")
	WebElement closeLeague;
	
	@FindBy( xpath = "//*[@id='TicketTabs']/li[1]/a")
	WebElement singleTab;
	
	@FindBy( xpath = ".//*[@id='TicketTabs']/li[3]/a")
	WebElement systemTab;
	
	@FindBy( css = ".ticket-item:nth-child(1) > input")
	WebElement stakeField1;
	
	@FindBy( css = ".ticket-item:nth-child(2) > input")
	WebElement stakeField2;
	
	@FindBy( css = ".ticket-item:nth-child(3) > input")
	WebElement stakeField3;
	
	@FindBy( css = "#PopUp > div > div.message-box-content > table > tbody > tr:nth-child(1) > td:nth-child(3)")
	WebElement messageSingle;
	
	@FindBy( css = "label[class='custom-checkbox']>span")
	WebElement plusOneSystem;
	
	@FindBy( css = ".ticket-item:nth-child(1)")
	WebElement systemFirstMatch;
	
	@FindBy( css = ".ticket-item:nth-child(1) > select > option[value='0']")
	WebElement systemFirstMatchInsertBanker;
	
	@FindBy( xpath = "//*[@id='PopularOpener']/span[3]")
	WebElement SBTreePopularSports;
	
	@FindBy( xpath = "//*[@id='Cat27']/span[3]")
	WebElement SBTreeSoccer;

	@FindBy( xpath = "//*[@id='Cat30']/span[3]")
	WebElement SBTreeBasketball;
	
	@FindBy( xpath = "//*[@id='Cat29']/span[3]")
	WebElement SBTreeTennis;
	
	@FindBy( xpath = "//*[@id='Cat36']/span[3]")
	WebElement SBTreeHockey;
	
	@FindBy( xpath = "//*[@id='Cat43']/span[3]")
	WebElement SBTreeRugby;
	
	@FindBy( xpath = "//*[@id='Cat37']/span[3]")
	WebElement SBTreeHandball;
	
	@FindBy( xpath = "//*[@id='Cat35']/span[3]")
	WebElement SBTreeGolf;
	
	@FindBy (css = "div[class='btn btn-green']>input[value='Place Bet']")
	WebElement PlaceBetPopup;
	
	@FindBy (css = "button[class='freebet-popup__close']")
	WebElement closeFreebetPopup;
	
	@FindBy (css = "div[class='freebet__information']")
	WebElement chooseFreebet;
	
	@FindBy (css = "div[class='freebet__select'] > ul > li:nth-child(2)")
	WebElement selectFreebet;
	
	
	public HomePage(WebDriver driver) {

		this.driver = driver;

	}

	public void login(String user, String pass) throws Exception  {		
		waitUntilElementIsVisible(usernameField);
		usernameField.sendKeys(user);
		passwordField.sendKeys(pass);
		loginButton.click();
		Thread.sleep(2000);
	}

	public void logout(){
		waitUntilElementIsVisible(logoutButton);
		logoutButton.click();
	}
	
	public void closePopupIncorrectUsername() throws Exception{
		
		waitUntilElementIsVisible(message);
		String text = message.getText();
		if (!text.equals("Specified username or password is incorrect")) {
			throw new Exception("ERROR !!!! No pop-up that this user doesn't exist.");
		}
		else{
			closePopup.click();
			}
	}
	
	public void openQALeague2() throws InterruptedException{
		
		Thread.sleep(4000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("Categories.OpenLeague("+GeneralTest.prop.getProperty("QALeague2_ID")+")");
		
	}
	
	public void openQALeague1() throws InterruptedException{
		Thread.sleep(3000);
		waitUntilElementIsVisible(sportsbookTree);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("Categories.OpenLeague("+GeneralTest.prop.getProperty("QALeague1_ID")+")");
		
	}
	
	public void selectQAMatch1_odd1(){
	waitUntilElementIsVisible(sportsbookTree);
    waitUntilElementIsVisible(QAMatch1_odd1);	
    QAMatch1_odd1.click();
	}
	
	
    public void selectQAMatch1_odd5(){
		
	waitUntilElementIsVisible(QAMatch1_odd5);
	QAMatch1_odd5.click();
	}

    public void selectQAMatch1_odd5_6(){
		
    waitUntilElementIsVisible(QAMatch1_odd5_6);
    QAMatch1_odd5_6.click();
    }
    
    public void selectQAMatch1_odd7(){
	
	waitUntilElementIsVisible(QAMatch1_odd7);
	QAMatch1_odd7.click();
	}
	
	public void selectQAMatch2_odd1(){
		
	waitUntilElementIsVisible(QAMatch2_odd1);
	QAMatch2_odd1.click();
	}
	
	public void selectQAMatch2_odd7(){
		
	waitUntilElementIsVisible(QAMatch2_odd7);
	QAMatch2_odd7.click();
	}
	
	public void selectQAMatch2_odd5(){
		
	waitUntilElementIsVisible(QAMatch2_odd5);
	QAMatch2_odd5.click();
	}
	
	public void insertStake (String value){
		
		waitUntilElementIsVisible(stakeField);
		stakeField.sendKeys(value);
		
	}
	
	
	public void clickBet(){
		waitUntilElementIsVisible(betButton);
		betButton.click();
		
	}
	
	
public void closePopupTicketPlacedSuccessfully() throws Exception{
		
		waitUntilElementIsVisible(message);
		String text = message.getText();
		if (!text.startsWith("Ticket is placed successfully.")) {
			System.out.print(text);
			throw new Exception("ERROR !!!! Ticket can not be placed.");
		}
		else{
			closePopup.click();
			}
	}


public void closePopupTicketPlacedSuccessfullySingle() throws Exception{
	
		waitUntilElementIsVisible(closePopupOK);
		closePopupOK.click();
		
}


public void closePopupTicketPlacedSuccessfullyNonStop() throws Exception{
	
	waitUntilElementIsVisible(message);
	String text = message.getText();
	if (!text.startsWith("Ticket is placed successfully.")) {
		System.out.print(text);
		throw new Exception("ERROR !!!! Ticket can not be placed.");
	}
	else{
		closePopupOK.click();
		}
	
}
public int checkUserBalance(){
	
	waitUntilElementIsVisible(userBalance);
	String Balance = userBalance.getText();
	int BalanceInt;
	String[] parts = Balance.split("\\.");
	String part1 = parts[0];

	String secondpart = "";

	if (part1.contains(",")) {

		secondpart = part1.replace(",", "");

	} else {
		secondpart = part1;
		
	
}
	BalanceInt = Integer.parseInt(secondpart);
	return BalanceInt;

}

public void closeLeague() throws InterruptedException{
	waitUntilElementIsVisible(closeLeague);
	closeLeague.click();
	Thread.sleep(1000);
	
}

public void singleTab(){
	
	waitUntilElementIsVisible(singleTab);
	singleTab.click();
}

public void systemTab(){
	
	waitUntilElementIsVisible(systemTab);
	systemTab.click();
}

public void insertStake1(String value){
	
	waitUntilElementIsVisible(stakeField1);
	stakeField1.sendKeys(value);
}

public void insertStake2(String value){
	
	waitUntilElementIsVisible(stakeField2);
	stakeField2.sendKeys(value);
}

public void insertStake3(String value){
	
	waitUntilElementIsVisible(stakeField3);
	stakeField3.sendKeys(value);
}

public void clearStake1(){
	
	waitUntilElementIsVisible(stakeField1);
	stakeField1.clear();
}

public void clearStake2(){
	
	waitUntilElementIsVisible(stakeField2);
	stakeField2.clear();
}

public void clearStake3(){
	
	waitUntilElementIsVisible(stakeField3);
	stakeField3.clear();
}

public float minBetAmount(){
	
	float MinBetAmount;
	waitUntilElementIsVisible(message);
	String msg = message.getText().substring(27, 31);
	MinBetAmount=Float.parseFloat(msg);
	
	return MinBetAmount;
	
	
}
public void closePopupMinBetAmount() throws Exception{
	
	waitUntilElementIsVisible(message);
	String text = message.getText();
	if (!text.startsWith("Minimum amount of stake is")) {
		throw new Exception("ERROR !!!! Min bet amount is not shown");
	}
	else{
		closePopupOK.click();
		}
}

public void clearStake(){
	
	waitUntilElementIsVisible(stakeField);
	stakeField.clear();
}

public void plusOneSystem(){
	
	waitUntilElementIsVisible(plusOneSystem);
	plusOneSystem.click();
}

public void systemMatchInsertBanker(){
	
	waitUntilElementIsVisible(systemFirstMatch);
	systemFirstMatchInsertBanker.click();
}

public void closePopupPleaseAuthorize() throws Exception{
	
	waitUntilElementIsVisible(message);
	String text = message.getText();
	if (!text.equals("Your session has expired. Please log in again.")) {
		throw new Exception("ERROR !!!! No pop-up that user is not authorized");
	}
	else{
		closePopupOK.click();
		}
}


public void closePopupOddExistOnTicket() throws Exception{
	
	waitUntilElementIsVisible(message);
	String text = message.getText();
	if (!text.equals("Impossible to add selected event, it already exists in ticket.")) {
		throw new Exception("ERROR !!!! Problem with non-stop ticket");
	}
	else{
		closePopupOK.click();
		}
}

public void clearBetslip () {
	
	waitUntilElementIsVisible(clearBetslipButton);
	clearBetslipButton.click();
}
public void confirmBetslipClear () {
	
	waitUntilElementIsVisible(confirmClear);
	confirmClear.click();
}
public void openPopular(){
	
	waitUntilElementIsVisible(SBTreePopularSports);
	SBTreePopularSports.click();
	
}

public void closePopular(){
	
	waitUntilElementIsVisible(closeLeague);
	SBTreePopularSports.click();
}

public void openSoccer(){
	
	waitUntilElementIsVisible(SBTreeSoccer);
	SBTreeSoccer.click();
}

public void closeSoccer(){
	
	waitUntilElementIsVisible(closeLeague);
	SBTreeSoccer.click();
}

public void openBasketball(){
	
	waitUntilElementIsVisible(SBTreeBasketball);
	SBTreeBasketball.click();
}

public void closeBasketball(){
	
	waitUntilElementIsVisible(closeLeague);
	SBTreeBasketball.click();
}

public void openTennis(){
	
	waitUntilElementIsVisible(SBTreeTennis);
	SBTreeTennis.click();
}

public void closeTennis(){
	
	waitUntilElementIsVisible(closeLeague);
	SBTreeTennis.click();
}

public void openHockey(){
	
	waitUntilElementIsVisible(SBTreeHockey);
	SBTreeHockey.click();
}

public void closeHockey(){
	
	waitUntilElementIsVisible(closeLeague);
	SBTreeHockey.click();
}

public void openRugby(){
	
	waitUntilElementIsVisible(SBTreeRugby);
	SBTreeRugby.click();
}

public void closeRugby(){
	
	waitUntilElementIsVisible(closeLeague);
	SBTreeRugby.click();
}


public void openHandball(){
	
	waitUntilElementIsVisible(SBTreeHandball);
	SBTreeHandball.click();
}

public void closeHandball(){
	
	waitUntilElementIsVisible(closeLeague);
	SBTreeHandball.click();
}

public void openGolf(){
	
	waitUntilElementIsVisible(SBTreeGolf);
	SBTreeGolf.click();
}

public void closeGolf(){
	
	waitUntilElementIsVisible(closeLeague);
	SBTreeGolf.click();
}

public void virtualsTab(){
	
	waitUntilElementIsVisible(virtualsTab);
	virtualsTab.click();
}

public void liveTab(){
	
	waitUntilElementIsVisible(liveTab);
	liveTab.click();
}


public void historyTab(){
	
	waitUntilElementIsVisible(historyTab);
	historyTab.click();
}

public void closePopupTicketWasRejected() throws Exception{
	
	waitUntilElementIsVisible(message);
	String text = message.getText();
	if (!text.equals("Ticket was rejected!")) {
		throw new Exception("ERROR !!!! Limit Stric doesn't work");
	}
	else{
		closePopupOK.click();
		}
}

public void closePopupOddsWereChanged() throws Exception{
	
	waitUntilElementIsVisible(messageChanged);
	String text = messageChanged.getText();
	if (!text.equals("Following odd(s) have been changed:")) {
		throw new Exception("ERROR !!!! Limit AutoDecrease doesn't work");
	}
	
}
public void placeBetPopup() {
	
	waitUntilElementIsVisible(PlaceBetPopup);
	PlaceBetPopup.click();
}

public void closePopupTimeForAcceptingPassed() throws Exception{
	
	waitUntilElementIsVisible(message);
	String text = message.getText();
	if (!text.equals("The time period for accepting the ticket has expired!")) {
		throw new Exception("ERROR !!!! Time for accepting not working");
	}
	else{
		closePopupOK.click();
		}
}

public void closePopupNoMoneyOnBalance() throws Exception{
	
	waitUntilElementIsVisible(message);
	String text = message.getText();
	if (!text.equals("You don't have enough balance!")) {
		throw new Exception("ERROR !!!! Error with the balance");
	}
	else{
		closePopupOK.click();
		}
}

public void closePopupMatchBeenDisabled() throws Exception{
	
	waitUntilElementIsVisible(messageChanged);
	String text = messageChanged.getText();
	if (!text.equals("Match has been disabled!")) {
		throw new Exception("ERROR !!!! Disabled Match can be placed");
	}
	else{
		closePopupOK.click();
		}
}

public void closeFreebetPopup(){
	
	waitUntilElementIsVisible(closeFreebetPopup);
	closeFreebetPopup.click();
}

public void chooseFreebet(){
	
	waitUntilElementIsVisible(chooseFreebet);
	chooseFreebet.click();
}

public void selectFreebet(){
	
	waitUntilElementIsVisible(selectFreebet);
	selectFreebet.click();
}


public void closePopupConditionCoupon() throws Exception{
	
	waitUntilElementIsVisible(message);
	String text = message.getText();
	if (!text.equals("Limit violation happen. You are not able to place this ticket")) {
		throw new Exception("ERROR !!!! Coupon conditions are not working as expected");
	}
	else{
		closePopupOK.click();
		}
}

public int numberOfSuccesfullyPlacedSingles() throws Exception{
	
	Thread.sleep(2000);
	int number;
	number=driver.findElements(By.xpath("//div[contains(@class, 'statusiconsingleticket success')]")).size();
	
	return number;
}

public void make_match_started(){
	
	try {

		conn = DriverManager.getConnection("jdbc:mysql://"+GeneralTest.prop.getProperty("dbIp")+":"+GeneralTest.prop.getProperty("dbPort")+"/sportsbook", GeneralTest.prop.getProperty("dbUser"), GeneralTest.prop.getProperty("dbPass"));
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("UPDATE matches SET start_date= current_time() - interval '30' second  WHERE external_id = '100000024';");
	} catch (Exception e) {
		e.printStackTrace();
	}

}

public void return_match_date(){
	
	try {

		conn = DriverManager.getConnection("jdbc:mysql://"+GeneralTest.prop.getProperty("dbIp")+":"+GeneralTest.prop.getProperty("dbPort")+"/sportsbook", GeneralTest.prop.getProperty("dbUser"), GeneralTest.prop.getProperty("dbPass"));
			Statement stmt = conn.createStatement();
			stmt.executeUpdate("UPDATE matches SET start_date='2021-01-01 00:00:00' WHERE external_id = '100000024';");
		} catch (Exception e) {
			e.printStackTrace();
		}

}
}
