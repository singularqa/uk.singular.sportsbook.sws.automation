package automation_sws_pages;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import webdriver.GeneralTest;

public class HistoryPage extends GeneralTestPage{
	private static Connection conn;
	@FindBy(xpath = ".//*[1]/div[1]/ul[2]/li[6]/a")
	WebElement cashoutButton;
	
	
	@FindBy(xpath = "//*[@id='PopUp']/div/div[3]/div[2]/input")
	WebElement acceptCashout;
	
	@FindBy( xpath = ".//*[@id='PopUp']/div/div[2]")
	WebElement message;
	
	@FindBy( css = "#PopUp > div > div.message-box-actions > div > input")
	WebElement closePopupOK;
	
	
	@FindBy( xpath = ".//*[1]/div[1]/ul[2]/li[5]/a")
	WebElement addOddButton;
	
	@FindBy( xpath = "(//div[2]/div/div/ul/li[2])[1]")
	WebElement ticket_id;
	

	
	
	public HistoryPage(WebDriver driver) {

		this.driver = driver;

	}
	
	public void cashOutButton() throws Exception{
		
		try{
		waitUntilElementIsVisible(cashoutButton);
		cashoutButton.click();
		}
		
		catch (Exception e) {
			throw new Exception("Cashout not available for the match at the moment");
		}
		
	}
	
	public void makeCashout(){
		
		waitUntilElementIsVisible(acceptCashout);
		acceptCashout.click();
		
	}
	
	
public void closePopupCashoutSuccessful() throws Exception{
		
		waitUntilElementIsVisible(message);
		String text = message.getText();
		if (text.startsWith("Cash out of amount")) {
		
			closePopupOK.click();
			
		}else
			if (text.startsWith("Cashout Amount was changed")){
				
				makeCashout();
				closePopupCashoutSuccessful();
				
			}
		else{
			System.out.print(text);
			throw new Exception("ERROR !!!! Cashout doesn't work!");
			}
	}

public void addOdd(){
	waitUntilElementIsVisible(addOddButton);
	addOddButton.click();
	
}


public void closePopupCannotMixVirtualAndPrematch() throws Exception{
	
	waitUntilElementIsVisible(message);
	String text = message.getText();
	if (text.startsWith("You can't place Virtual sport odd with Regular sport odd")) {
	
		closePopupOK.click();
		
	}
	else{
		System.out.print(text);
		throw new Exception("ERROR !!!! Problem with mixing virtuals and prematch odds");
		}
}

public void resolve_ticket_winnig(){
	
	waitUntilElementIsVisible(ticket_id);
	String Ticket = ticket_id.getText();
	int T_id = Integer.parseInt(Ticket.substring(4));
	try {

		conn = DriverManager.getConnection("jdbc:mysql://"+GeneralTest.prop.getProperty("dbIp")+":"+GeneralTest.prop.getProperty("dbPort")+"/sportsbook", GeneralTest.prop.getProperty("dbUser"), GeneralTest.prop.getProperty("dbPass"));
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("Update ticket_odds set status=2, processed=0 where ticket_id =" + T_id);
	} catch (Exception e) {
		e.printStackTrace();
	}
}
	

	
public void closePopupForbidenToAddAdditionalOdds() throws Exception{
	
	waitUntilElementIsVisible(message);
	String text = message.getText();
	if (text.startsWith("Forbidden to add additional odds to ticket")) {
	
		closePopupOK.click();
		
	}
	else{
		System.out.print(text);
		throw new Exception("ERROR !!!! Problem adding additional odds to resolved ticket");
		}
}

	
}
