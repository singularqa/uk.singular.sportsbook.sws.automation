package automation_sws_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;




public class LivePage extends GeneralTestPage{
	
	
	@FindBy(css = "label[for='AcceptChanges']")
	WebElement acceptChanges;
	
	public LivePage(WebDriver driver){

		this.driver = driver;

	}
	
	public void acceptChanges(){
		waitUntilElementIsVisible(acceptChanges);
		acceptChanges.click();
		
	}
	public void select_live_odds (int number_of_odds){
		WebDriverWait wait = new WebDriverWait(driver, 35);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("li[class=action-toggle]")));
		int counter=0;
		int columnCount=0;
		int sportsCount=driver.findElements(By.xpath("//*[@id='overView_panel']/div")).size();
		
		breakloop:
		for(int j=1; j <= sportsCount; j++){
			
			columnCount=0;
			columnCount=driver.findElements(By.xpath("//*[@id='overView_panel']/div["+j+"]/.//table/tbody/tr")).size();
			
			for(int i=1; i <= columnCount; i++){
						
				try{
		
					String vrednost=driver.findElement(By.xpath("//*[@id='overView_panel']/div["+j+"]/.//table/tbody/tr["+i+"]/td[9]")).getText();
					
					
					if(!vrednost.equals("")){
			driver.findElement(By.xpath("//*[@id='overView_panel']/div["+j+"]/.//table/tbody/tr["+i+"]/td[9]")).click();
			System.out.println(vrednost);
			counter++;
					}
		
				   }

	catch (Exception e) {
		// TODO: handle exception
	}
		
					if(counter>=number_of_odds){
	
						break breakloop;
								}}}
		
		if(counter<1){
		System.out.println("There are no live odds at the moment!");
		}
	}
	
	public int numberOfSuccesfullyPlacedSingles() throws Exception{
		
		Thread.sleep(10000);
		int number;
		number=driver.findElements(By.xpath("//div[contains(@class, 'statusiconsingleticket success')]")).size();
		
		return number;
	}
	
	public void select_live_odds_Starting_from_seconds_match (int number_of_odds){
		WebDriverWait wait = new WebDriverWait(driver, 35);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("li[class=action-toggle]")));
		int counter=0;
		int columnCount=0;
		int sportsCount=driver.findElements(By.xpath("//*[@id='overView_panel']/div")).size();
		
		breakloop:
		for(int j=1; j <= sportsCount; j++){
			
			columnCount=0;
			columnCount=driver.findElements(By.xpath("//*[@id='overView_panel']/div["+j+"]/.//table/tbody/tr")).size();
			
			for(int i=2; i <= columnCount; i++){
						
				try{
		
					String vrednost=driver.findElement(By.xpath("//*[@id='overView_panel']/div["+j+"]/.//table/tbody/tr["+i+"]/td[9]")).getText();
					
					
					if(!vrednost.equals("")){
			driver.findElement(By.xpath("//*[@id='overView_panel']/div["+j+"]/.//table/tbody/tr["+i+"]/td[9]")).click();
			System.out.println(vrednost);
			counter++;
					}
		
				   }

	catch (Exception e) {
		// TODO: handle exception
	}
		
					if(counter>=number_of_odds){
	
						break breakloop;
								}}}
		
		if(counter<1){
		System.out.println("There are no live odds at the moment!");
		}
	}

}
