package automation_sws_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

public class VirtualsPage extends GeneralTestPage{
	
	@FindBy(xpath = ".//*[@id='VirtualSports']/a[2]")
	WebElement betradarVirtuals;
	
	@FindBy(css = "li[data-sport='league_mode']")
	WebElement VFL;
	
	@FindBy(css = "li[data-sport='nations_cup']")
	WebElement VFL_NationsCup;
	
	@FindBy(css = "li[data-sport='euro_cup']")
	WebElement VFL_EuroCup;
	
	@FindBy(css = "li[data-sport='tennis']")
	WebElement VTL;
	
	@FindBy(css = "li[data-sport='basketball']")
	WebElement VBL;
	
	@FindBy(xpath = "html/body/div/div[4]/div[4]/div[8]/div/div[2]/div/div[2]/div/div/table/tbody/tr[1]/td[3]/a")
	WebElement odd_first_match_VFL;
	
	@FindBy(xpath = "html/body/div/div[4]/div[4]/div[8]/div/div[2]/div/div[2]/div/div/table/tbody/tr[3]/td[4]/a")
	WebElement odd_second_match_VFL;
	
	@FindBy(xpath = "html/body/div/div[4]/div[4]/div[8]/div/div[2]/div/div[2]/div/div/table/tbody/tr[5]/td[3]/a")
	WebElement odd_third_match_VFL;
	
	@FindBy(xpath = "html/body/div/div[4]/div[4]/div[8]/div/div[2]/div/div[2]/div/div/table/tbody/tr[1]/td[3]/a")
	WebElement odd_first_match_VBL;
	
	@FindBy(xpath = "html/body/div/div[4]/div[4]/div[8]/div/div[2]/div/div[2]/div/div/table/tbody/tr[3]/td[2]/a")
	WebElement odd_second_match_VBL;
	
	@FindBy(xpath = "html/body/div/div[4]/div[4]/div[8]/div/div[2]/div/div[2]/div/div/table/tbody/tr[5]/td[3]/a")
	WebElement odd_third_match_VBL;
	
	@FindBy(xpath = "html/body/div/div[4]/div[4]/div[8]/div/div[2]/div/div[2]/div/div/table/tbody/tr[1]/td[3]/a")
	WebElement odd_first_match_VTL;
	

	public VirtualsPage(WebDriver driver){

		this.driver = driver;

	}
	public void betradarVirtuals() throws InterruptedException{
		
		waitUntilElementIsVisible(betradarVirtuals);
		betradarVirtuals.click();
	}
	
	public void vfl(){
		waitUntilElementIsVisible(VFL);
		VFL.click();
		
	}
	
	public void vfl_nationsCup(){
		waitUntilElementIsVisible(VFL_NationsCup);
		VFL_NationsCup.click();
		
	}
	
	public void vfl_euroCup(){
		waitUntilElementIsVisible(VFL_EuroCup);
		VFL_EuroCup.click();
		
	}
	
	public void vtl(){
		waitUntilElementIsVisible(VTL);
		VTL.click();
		
	}
	public void vbl(){
		waitUntilElementIsVisible(VBL);
		VBL.click();
		
	}
	public void vfl_odd_first_match(){
		waitUntilElementIsVisible(odd_first_match_VFL);
		odd_first_match_VFL.click();
		
	}
	public void vfl_odd_second_match(){
		waitUntilElementIsVisible(odd_second_match_VFL);
		odd_second_match_VFL.click();
		
	}
	public void vfl_odd_third_match(){
		waitUntilElementIsVisible(odd_third_match_VFL);
		odd_third_match_VFL.click();
		
	}
	public void vbl_odd_first_match(){
		waitUntilElementIsVisible(odd_first_match_VBL);
		odd_first_match_VBL.click();
		
	}
	public void vbl_odd_second_match(){
		waitUntilElementIsVisible(odd_second_match_VBL);
		odd_second_match_VBL.click();
		
	}
	public void vbl_odd_third_match(){
		waitUntilElementIsVisible(odd_third_match_VBL);
		odd_third_match_VBL.click();
		
	}
	
	public void vtl_odd_first_match(){
		waitUntilElementIsVisible(odd_first_match_VTL);
		odd_first_match_VTL.click();
		
	}
}
